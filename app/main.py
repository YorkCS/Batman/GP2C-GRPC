import os
from time import sleep
from sys import stdout
from gp2 import Compiler
from grpc import server, Server
from concurrent.futures import ThreadPoolExecutor

from gp2c_pb2 import *
from gp2c_pb2_grpc import *


class Service(GP2CServicer):

    def __init__(self, compiler):
        self._compiler = compiler

    def Compile(self, program, context):
        if program.content is None or program.content == '':
            return CProgram(error='A program must be provided.')

        files, error = self._compile(program.content)

        if (files is None):
            return CProgram(error=error)

        compiled = []
        for k, v in files.items():
            compiled.append(CodeFile(name=k, content=v))

        return CProgram(code=SourceCode(files=compiled))

    def _compile(self, program):
        try:
            return self._compiler.compile(program)
        except BaseException as e:
            print(e)
            stdout.flush()
            return None, 'An unexpected error occured.', 0


class Factory(object):

    @staticmethod
    def make(service = None):
        MB = 1024 * 1024
        GRPC_CHANNEL_OPTIONS = [('grpc.max_message_length', 256 * MB), ('grpc.max_receive_message_length', 256 * MB)]

        s = server(ThreadPoolExecutor(max_workers=16), options=GRPC_CHANNEL_OPTIONS)
        add_GP2CServicer_to_server(service or Factory._make_service(), s)
        s.add_insecure_port('[::]:9111')

        return s

    @staticmethod
    def _make_service(script_path = None):
        return Service(
            Compiler(
                script_path or 'run.sh',
                int(os.environ.get('GP2_COMPILER_TIMEOUT', '0')) or None
            )
        )


def main():
    s = Factory.make()
    s.start()

    try:
        while True:
            sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        s.stop(0)


if __name__ == '__main__':
    main()
