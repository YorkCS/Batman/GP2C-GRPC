# GP2C GRPC

## Prereqs

Any modern Linux or Mac running Docker 2018 CE or greater will suffice.

## Running

Usage of the GP2C GRPC Docker image is easy. Simply start the daemon and off we go!

```bash
$ docker run -d -p 9111:9111 registry.gitlab.com/yorkcs/batman/gp2c-grpc
```

### Configuration

It is also possible to the `GP2_FLAGS` environment variables.

For example:

```bash
$ docker run -e GP2_FLAGS='-f' -d -p 9111:9111 registry.gitlab.com/yorkcs/batman/gp2c-grpc
```

## Building

In standard Docker fashion:

```bash
$ docker build -t registry.gitlab.com/yorkcs/batman/gp2c-grpc:latest .
```

And then to publish:

```bash
$ docker push registry.gitlab.com/yorkcs/batman/gp2c-grpc
```
